package com.statefund.example.cas_example.protected_restful;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.HashMap;

@RestController
public class RestfulExample {

    @GetMapping("/test_cas")
    public ResponseEntity saveInstance() throws IOException {
        HashMap instance = new HashMap();
        instance.put("data", "is_done");
        return new ResponseEntity(instance, HttpStatus.OK);
    }

}
